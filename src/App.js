import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'
import 'bootstrap/dist/css/bootstrap.min.css';

const initialList = [ ];

const App = () => {
    const [list, setList] = React.useState(initialList);
    const [name, setName] = React.useState('');

    //    var alfaNum = false;

    function handleChange(event) {
	setName(event.target.value);
    }
    
    function handleAdd(ev) {
	const newList = list.concat({ name, id:uuidv4() });
	var results = newList.filter(function (entry) { return entry.name === name; });
	
	if (name == "" || /[^0-9a-zA-Z]/.test(name) || results.length == 2 )  {
	    alert("Caracteres no alfanumericos y palabras repetidas no se aceptan")
//	    var alfaNum = !!(false);
	} else {
	    setList(newList);
	    setName('');
	}
    }

    function removeItem(id) {
	let currentList = list;
	let listToUpdate = currentList.filter(element => element.id != id);
	setList(listToUpdate);
    }
    
    const keyDown = (ev) => {
	if (ev.keyCode == 13) {
	    handleAdd();
	}
    }

    const List = ({ list }) => (

	<Card style={{ width: '18rem' }}>
	    <ListGroup variant="flush">
		{list.map((item) => (
		    <ListGroup.Item key={item.id} action onClick={()=>removeItem(item.id)}>{item.name}</ListGroup.Item>
		))}
	    </ListGroup>
	</Card>
    )
    
    return (
	<div>
	    <Form onSubmit={e => e.preventDefault()}>
		<Form.Group controlId="formTextInput">
		    <Form.Label>
			Ingrese una palabra: 
		    </Form.Label>
		    <input type="text" value={name} onChange={handleChange} onKeyDown={keyDown} />

		</Form.Group>
		<Button variant="primary" type="button" onClick={handleAdd} >
		Agregar
		</Button>

	    </Form>
	    <List list={list}/>
	    	   
	</div>
    );
};

export default App;
